package Oauthservlet.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class OauthUtils {
	private static Properties prop = new Properties();
	static {
		try {
			InputStream is = new FileInputStream("D:\\TFG\\OAuthentication\\src\\main\\resources\\application.properties");
			prop.load(is);
		} catch(IOException e) {
			System.out.println(e.toString());
		}
	}
	public static final String urlDBSqlite = prop.getProperty("url.db.sqlite.users");
	public static final String tokenExpiration = prop.getProperty("oauth.token.expiration");
}
