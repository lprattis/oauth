package Oauthservlet;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Oauthservlet.db.sqlite.manager.DatabaseManager;
import Oauthservlet.domain.Token;
import Oauthservlet.domain.User;
import Oauthservlet.utils.OauthUtils;


@WebServlet("/hello")
public class Oauthservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DatabaseManager DBmanager;
	
	@Override
	public void init() {
		DBmanager = new DatabaseManager(OauthUtils.urlDBSqlite);
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher view = req.getRequestDispatcher("WEB-INF/html/loginForm.html");
		view.include(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		User authUser = DBmanager.login(req.getParameter("username"), req.getParameter("password"));
		if(authUser != null) {
			Token token = DBmanager.createToken(authUser);
			StringBuilder url = new StringBuilder("http://localhost:33333/staticServlet/Container");
			url.append("#token=").append(token.getToken()).append("&userName=").append(authUser.getName());
			url.append("&role=").append(authUser.getRole()).append("&id=").append(authUser.getId());
			resp.sendRedirect(resp.encodeRedirectURL(url.toString()));
//			RequestDispatcher view = req.getRequestDispatcher("WEB-INF/html/loginFormOk.html");
//			view.include(req, resp);
		}else {
			/**
			 * retornar un error d'autenticació
			 */
//			resp.sendError(401, "Usuari no trobat");
			RequestDispatcher view = req.getRequestDispatcher("WEB-INF/html/loginFormError.html");
			view.include(req, resp);
		}
	}
}
